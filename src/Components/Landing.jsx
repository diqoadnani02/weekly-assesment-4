import React from 'react';
import './style.css'

const Landing = () => {
    return (
        <div>
            <div className="container">
                <div className="container-1">
                    <div className="text-container">
                        <h1>GOT MARKETING?<br/> ADVANCE YOUR <br/>BUSINESS INSIGHT</h1>
                        <p>Fill out the receive and receive our <br/> award winning newsletter.</p>
                    </div>
                </div>
                <div className="container-2">
                    <form>
                        <div className="name-container">
                            <label htmlFor="name">Name</label>
                            <input type="text" id="name"></input>
                        </div>
                        <div className="email-container">
                            <label htmlFor="email">Email</label>
                            <input type="email" id="email"></input>
                        </div>
                        <button>Sign Me Up</button>
                    </form>
                </div>
            </div>
        </div>
    );
}

export default Landing;


// i give up, please forgive me